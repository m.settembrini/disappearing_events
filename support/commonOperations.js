const toStrings = (element$) => Cypress._.map(element$, 'textContent'); 
const removeLetters = (texts) => Cypress._.map(texts, e => e.replace(/[^0-9.]/g, ''));
const removeCommas = (texts) => Cypress._.map(texts, e => e.replace(',', ''))
const removePoints = (texts) => Cypress._.map(texts, e => e.replace('.', '')); 
const toNumbers = (texts) => Cypress._.map(texts, Number); 
const multiply = (numbers) => Cypress._.reduce(numbers, (a, b) => ((a * 100) * (b * 100)) / 10000).toFixed(2)
const multiplyUncut = (numbers) => Cypress._.reduce(numbers, (a, b) => ((a * 100) * (b * 100)) / 10000)
const round = (odd) => Cypress._.round(odd, 2)

const commonOperations = {

    /* Obtain correct total odd for multiple bet, given the selected odds DOM elements 
        Usage Example:
        cy.get(homeElements.genericSelectedOdd).then(multiplyOdds)
    */
    multiplyOdds: Cypress._.flow([toStrings, toNumbers, multiply/*, round*/]),

    //multiplyOddsNew: Cypress._.flow([toStrings, removeLetters, toNumbers, multiply/*, round*/]),
    multiplyOddsNew: Cypress._.flow([toStrings, removeLetters, toNumbers, multiply]),

    tidyUpNumber: Cypress._.flow([toStrings, removeLetters, toNumbers]),

    multiplyOddsUncut: Cypress._.flow([toStrings, toNumbers, multiplyUncut]),

    convertElementsToNumbers: Cypress._.flow([toStrings, removeCommas, toNumbers]),

    convertElementsToNumbersBalance: Cypress._.flow([toStrings, removePoints, toNumbers]),

    getRandomBetRow() {
        return parseInt(Math.random() * (10 - 5) + 5)
    },

    convertElementsToText: Cypress._.flow([toStrings]),

}
export default { ...commonOperations }