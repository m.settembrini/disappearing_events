import utils from "../website/utils/utils"
const helpersMobile = {

    mobileLayout(url, headers=utils.HEADERS, viewport=utils.MOBILE.VIEWPORT.iphone_xr){
      cy.viewport(viewport)
      cy.visit(url, {})
    }
}
export default {...helpersMobile}