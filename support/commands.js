import 'cypress-iframe';

Cypress.Commands.add('iframeCustom', { prevSubject: 'element' }, ($iframe) => {
  return new Cypress.Promise((resolve) => {
    $iframe.ready(function () {
      resolve($iframe.contents().find('body'));
    })
  });
});

Cypress.Commands.add('dragAndDrop', (elementToDrag, newPosition) => {
  // elementToDrag and newPosition shouid both be element locators
  cy.get(elementToDrag).trigger('dragstart');
  cy.get(newPosition).trigger('drop');
  cy.get(elementToDrag).trigger('dragend');
})

Cypress.Commands.add('waitForSportTabToLoad', (tab) => {
  // Wait for the tab to load in the dedicated sport page
  cy.intercept({
    method: 'GET',
    url: '/_next/data/development/sport/Soccer/1/'+tab+'.json?sportSlug=Soccer&id=1',
  }).as('loadTab');
  cy.wait('@loadTab',{ requestTimeout: 10000, responseTimeout: 60000 }).its('response.statusCode').should('equal', 200)
})

Cypress.Commands.overwrite(
  'click',
  (originalFn, subject, positionOrX, y, options = {}) => {
    subject.click({force:true})
    return originalFn(subject, positionOrX, y, options)
  }
)

Cypress.Commands.add('trueClick', (elementToClick) => {
  cy.get(elementToClick).trigger('pointerdown');
  cy.get(elementToClick).trigger('mousedown');
  cy.get(elementToClick).trigger('pointerup');
  cy.get(elementToClick).trigger('mouseup');
  cy.get(elementToClick).trigger('click');
})

Cypress.Commands.add('updatePasswordInFixture', (newPassword) => {
  cy.fixture('users'+Cypress.env("target_env")+'.json').then((fixtureData) => {
    fixtureData.change_pwd_pass = newPassword;
    cy.writeFile('./cypress/fixtures/users_'+Cypress.env("target_env")+'.json', fixtureData);
  });
});

Cypress.Commands.add('addNewUserInFixture', (user, pass) => {
  cy.fixture('users_'+Cypress.env("target_env").toString().toLowerCase()).then((fixtureData) => {
    // Create new pair of json entries for the new created user
    const userKey = "user_"+user
    const passKey = "pass_"+user
    fixtureData = { ...fixtureData, [userKey]: user };
    fixtureData = { ...fixtureData, [passKey]: pass };
    cy.writeFile('./cypress/fixtures/users_'+Cypress.env("target_env").toString().toLowerCase()+'.json', fixtureData);
  });
});
