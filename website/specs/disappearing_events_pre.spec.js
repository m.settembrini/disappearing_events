/**
 * @author Matteo Settembrini <m.settembrini@logifuture.com>
 */
import helpersMobile from "../../support/helpersMobile";

beforeEach(() => {
    //helpersMobile.mobileLayout("https://sports.bet9ja.com/mobile/liveBetting");
    helpersMobile.mobileLayout("https://pre-sports.bet9ja.com/mobile/liveBetting");
    cy.wait(2000)
});
describe(
  "Logging getLive",
  { tags: ["stg", "regression"] },

  () => {

    it("Logging get live", function () {

        let nEvents = 0
        for (let i = 0; i < 10; i++) {
            //cy.intercept("GET", "https://sports.bet9ja.com/mobile/feapi/PalimpsestLiveAjax/GetLiveEventsV3?v_cache_version=1.250.8.175").as("getLive")
            cy.intercept("GET", "https://pre-sports.bet9ja.com/mobile/feapi/PalimpsestLiveAjax/GetLiveEventsV3?v_cache_version=1.250.8.175").as("getLive")
            cy.reload()
            cy.wait("@getLive").then((request)=> {
                let events = request.response.body.D.E
                for (let [id, event] of Object.entries(events)) {
                    if (event.EXTID.includes("ZM_")) {
                        delete events[id]
                    }
                }
                cy.task('log', Object.keys(events).length)
                cy.log(Object.keys(events).length)
                if(Object.keys(events).length != nEvents){
                    cy.log(events)
                    cy.task('log', events)
                    nEvents = Object.keys(events).length
                }
            })
            //cy.get("#upcoming-events_sid-1").scrollIntoView({ duration: 3000 })
            cy.wait(2000)
        }
    });
 
  }
);
