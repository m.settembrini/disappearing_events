const bespoke_stg_urls = {
  basepath: "https://stg-website.mamabet.cd",
  registration_URL: "https://stg-website.mamabet.cd/registration/client",
  casino_URL: "https://stg-website.mamabet.cd/casino/all",
  casino_explore_URL: "https://stg-website.mamabet.cd/casino/explore",
  sportsbook_URL: "https://stg-website.mamabet.cd/betting",
  sportsbook_explore_URL: "https://stg-website.mamabet.cd/betting/explore",
  live_soccer: "https://int-website.mamabet.cd/betting/live/soccer/3000001",
  soccer_URL: "/sport/soccer/1",
  basket_URL: "/sport/basketball/2",
  outrights_URL: "/sport/soccer/1/outrights",
  players_URL: "/sport/soccer/1/players",
  coupons_URL: "/sport/soccer/1/coupons",
  leagues_URL: "/sport/soccer/1/leagues",
  upcoming_URL: "/sport/soccer/1/upcoming",
  nba_league_URL:
    "https://stg-website.mamabet.cd/betting/matches/Basketball/2/NBA/762",
};
module.exports = bespoke_stg_urls;
