const bespoke_dev_urls = {
  registration_URL: "http://localhost:4000/client",
  //casino_shell_URL: "http://localhost:4000/casino",
  casino_URL: "http://localhost:4000/casino/all",
  casino_explore_URL: "http://localhost:4000/casino/explore",
  //homepage_URL: "http://localhost:4000", TODO: CHECK
  sportsbook_URL: "http://localhost:4000/betting",
  sportsbook_explore_URL: 'http://localhost:4000/betting/explore',
  soccer_URL: "/sport/soccer/1",
  basket_URL: "/sport/Basketball/2",
  outrights_URL: "/sport/Soccer/1/outrights",
  players_URL: "/sport/Soccer/1/players",
  coupons_URL: "/sport/Soccer/1/coupons",
  leagues_URL: "/sport/Soccer/1/leagues",
  upcoming_URL: "/sport/Soccer/1/upcoming",
  nba_league_URL: "http://localhost:4000/betting/matches/basketball/2/a1/1440692",
};
module.exports = bespoke_dev_urls;
