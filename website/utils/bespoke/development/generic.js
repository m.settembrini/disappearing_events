const bespoke_dev_generic_utils = {
    // Sports with STATS
    "sport_with_stats": [
        "Soccer",
        "Basketball",
        "Ice Hockey",
        "Badminton",
        "Beach Volley",
        "Handball",
    ],
    // Bet types
    "bet_types": {
        single_prematch: "SINGLE_PM",
        multiple_prematch: "MULTIPLE_PM",
        split_prematch: "SPLIT_PM",
        single_live: "SINGLE_LIVE",
        multiple_live: "MULTIPLE_LIVE",
        split_live: "SPLIT_LIVE",
        multiple_mixed: "MULTIPLE_MIXED"
    },
    // Casino categories ids
    "casino_categories": {
        all_id: '0',
        slots_id: '80018',
        new_id: '40013',
        popular_id: '40001',
    },
    "casino_games": {
        wolf_gold: '1700249',
        genie_wishes: '1700201',
        queen_cleopatra: '408000',
        escape_from_mars: '180208',
        baccarat: '180201'
    }
}
module.exports = bespoke_dev_generic_utils