const bespoke_int_urls = {
  basepath: "https://int-website.mamabet.cd",
  registration_URL: "https://int-website.mamabet.cd/registration/client",
  casino_URL: "https://int-website.mamabet.cd/casino/all",
  casino_explore_URL: "https://int-website.mamabet.cd/casino/explore",
  sportsbook_URL: "https://int-website.mamabet.cd/betting",
  sportsbook_explore_URL: "https://int-website.mamabet.cd/betting/explore",
  soccer_URL: "/sport/soccer/1",
  basket_URL: "/sport/Basketball/2",
  outrights_URL: "/sport/Soccer/1/outrights",
  players_URL: "/sport/Soccer/1/players",
  coupons_URL: "/sport/Soccer/1/coupons",
  leagues_URL: "/sport/Soccer/1/leagues",
  upcoming_URL: "/sport/Soccer/1/upcoming",
  nba_league_URL:
    "https://stg-website.mamabet.cd/betting/matches/Basketball/2/NBA/762",
};
module.exports = bespoke_int_urls;
