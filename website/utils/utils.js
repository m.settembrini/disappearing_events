const bespoke_dev_generic_utils = require("./bespoke/development/generic")
const bespoke_dev_urls = require("./bespoke/development/urls")
const bespoke_dev_users = require("./bespoke/development/users")
const bespoke_int_generic_utils = require("./bespoke/int/generic")
const bespoke_int_urls = require("./bespoke/int/urls")
const bespoke_stg_generic_utils = require("./bespoke/stg/generic")
const bespoke_stg_urls = require("./bespoke/stg/urls")

const utils = {
    MOBILE: {
        HEADERS: {},
        VIEWPORT: {
            iphone_xr : "iphone-xr",
        }
    },
    DRC: {
        DEV: {
            headers: { /*'user-agent': 'Mozilla/5.0 (Linux; Android 10; SM-N960F Build/QP1A.190711.020; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/103.0.5060.70 Mobile Safari/537.36 [FB_IAB/FB4A;FBAV/373.0.0.31.112;]'*/ },
            generic: bespoke_dev_generic_utils,
            urls: bespoke_dev_urls
        },
        INT: {
            headers: {},
            generic: bespoke_int_generic_utils,
            urls: bespoke_int_urls
        },
        STG: {
            headers: {},
            generic: bespoke_stg_generic_utils,
            urls: bespoke_stg_urls
        }
    },
    NG: {
        DEV: {
            headers: { /*'user-agent': 'Mozilla/5.0 (Linux; Android 10; SM-N960F Build/QP1A.190711.020; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/103.0.5060.70 Mobile Safari/537.36 [FB_IAB/FB4A;FBAV/373.0.0.31.112;]'*/ },
            generic: bespoke_dev_generic_utils,
            urls: bespoke_dev_urls
        },
        INT: {
            headers: {},
            generic: bespoke_int_generic_utils,
            urls: bespoke_int_urls
        },
        STG: {
            headers: {},
            generic: bespoke_stg_generic_utils,
            urls: bespoke_stg_urls
        }
    },
    CO: {
        DEV: {
            headers: { /*'user-agent': 'Mozilla/5.0 (Linux; Android 10; SM-N960F Build/QP1A.190711.020; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/103.0.5060.70 Mobile Safari/537.36 [FB_IAB/FB4A;FBAV/373.0.0.31.112;]'*/ },
            generic: bespoke_dev_generic_utils,
            urls: bespoke_dev_urls
        },
        INT: {
            headers: {},
            generic: bespoke_int_generic_utils,
            urls: bespoke_int_urls
        },
        STG: {
            headers: {},
            generic: bespoke_stg_generic_utils,
            urls: bespoke_stg_urls
        }
    }

}
module.exports = utils