const axios = require("axios");

exports.getRegistrationSwagger = async function getRegistrationSwagger(regSwaggerURL) {
  return axios.get(regSwaggerURL).then((parsed) => parsed.data);
};

// Parameters are the response to be validated, the schema, the api path (eg. /v1/schema), the status code of the response we want to validate
exports.validateSchema = async function validateSchema(
  response,
  schema,
  path,
  statusCode
) {
  const $RefParser = require("@apidevtools/json-schema-ref-parser"); // Library to dereference the schema
  const Ajv = require("ajv"); // Library to perform schema validation
  const addFormats = require("ajv-formats");
  const ajv = new Ajv({ allErrors: true });
  addFormats(ajv);
  ajv.addVocabulary(["example"]);

  cy.log("Starting Schema Validation...");
  console.log("Schema: ");
  console.log(schema);
  console.log("Response body: ");
  console.log(response);
  // Dereference schema
  let parser = new $RefParser();
  let resolvedSchema = await parser.dereference(schema);
  // Save the actual schema to validate, digging deep in the swagger using the parameters to navigate
  let pathSchema =
    resolvedSchema.paths[path].get.responses[statusCode].content[
      "application/json"
    ].schema;
  // Validate schema
  const validate = ajv.compile(pathSchema);
  const valid = validate(response);
  console.log("Errors: ");
  console.log(validate.errors);
  // Error logging. It logs the type of error, the error message, and where (roughly) the error has occurred
  if (!valid) {
    let errors = [];
    let ajvErrors = validate.errors;
    ajvErrors.forEach((ajvError) => {
      errors.push(
        "\n" +
          '"' +
          ajvError.keyword +
          '"' +
          " error - " +
          '"' +
          ajvError.message +
          '"' +
          " at element: " +
          '"' +
          ajvError.instancePath +
          '"'
      );
    });
    throw new Error(
      "Schema Validation Error - The following errors have occurred during validation of given schema against response body: " +
        errors
    );
  } else {
    cy.log("Schema Validation performed successfully");
  }
};
